#ifndef PACKAGE_H
#define PACKAGE_H

#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QString>
#include <QtCore/QTextStream>
#include <QtCore/QVariant>
#include <cutecat/core/cutesettings.h>
#include <cutecat/core/macros.h>
#include "config.h"

class Distro;
class Repository;

/** Abstract class of any package which can be added to a Repository.
 * 
 * Package-based objects should be created through Package::createByAttributes().
 * 
 * Constructors of this class and its derived classes are not public.
 * Please use Package::create() to create a new Package object.
 */
class Package
{
	friend class Distro; // Distro needs access to Package::_distro pointer
	
public:
	// Using large number to avoid collision with QDialog::DialogCode
	enum Type {
		Imported = 10000,
		Custom   = 10001,
	};
	
	static const int PackageRole = 32;
	static const int WidgetRole  = 33;
	
protected:
	ConfigSection* _confSection;
	Distro* _distro = 0;
	bool _hasErrors = false;
	bool _hasWarnings = false;
	
	/** Process environment which is used when running user scripts.
	 */
	QProcessEnvironment env;
	
	/** Base constructor.
	 * Derived classes must use it as base for their own constructors.
	 * 
	 * It is \c protected, so it can't be used directly.
	 * Please use Package::create() to create a new Package object.
	 */
	Package(const QString& name, ConfigSection* confSection);
	
public:
	virtual ~Package();
	
	ConfigSection* confSection() const;
	
	/** Returns named property as a QString. QStringLists are joined with commas.
	 * 
	 * @see hasProperty(), multiProp()
	 */
	QString prop(const QString& key, const QString& defaultValue="") const;
	
	/** Returns named propery as a QList.
	 * 
	 * @see prop()
	 */
	QStringList multiProp(const QString& key) const;
	
	/** Checks whether package has named property.
	 */
	bool hasProperty(const QString& key) const;
	
	/** Sets property to given value.
	 * 
	 * @see prop()
	 */
	void setProperty(const QString& key, const QString& value);
	void setProperty(const QString& key, const QStringList& value);

	QString name() const;
	void setName(const QString& name);
	
	/** Save current package in a separate file.
	 *
	 * Can be used from either RepositoryWindow or PackageWindow.
	 */
	void saveAs(const QString& fileName);
	
	/** Updates information about errors and warnings for the package.
	 */
	virtual void checkErrors() = 0;
	
	/** This method must create (or just copy) the package to given filepath.
	 * 
	 * It is called separately for each architecture from architectures().
	 * 
	 * Pointer to Repository is given for getting default variables like MaintainerEmail.
	 */
	virtual void build(const QString& arch, const QString& filePath, Repository *repository=0) = 0;
	
	/** Build package for given architecture as standard-named file in given directory.
	 * 
	 * This is a non-virtual wrapper about the other (virtual) version of \c build().
	 * It just constructs filename like \c helloworld_1.0_x64.deb inside \c dir
	 * and calls the virtual type-specific method to build package there.
	 */
	void build(const QString& arch, const QDir& dir=QDir());
	
	/** Returns \c 
	virtual bool canBeBuilt(const QString& arch);
	
	/** Returns \c true if file for architecture \c arch at \c filePath needs to be rebuilt.
	 * 
	 * This method is called with existing files only, see Repository::build().
	 */
	virtual bool needToRebuild(const QString& arch, const QString& filePath) const = 0;
	
	/** Runs pre-build script providing $SRC and $BUILD variables to it.
	 * 
	 * The script can write «key=value» pairs into given temporary file.
	 */
	void runPreBuildScript(const QString& arch);
	
	/** Runs post-build script probiding $SRC and $BUILD variables to it.
	 */
	void runPostBuildScript(const QString& arch);
	
	/** This method must return list of availiable architectures.
	 * 
	 * If list of architectures is given in «Architectures» property, it is used.
	 * Else, all available keys in Files or Archives are listed.
	 */
	virtual QStringList architectures() const = 0;
	
	/** Get the distro package belongs to.
	 *
	 * There is no setDistro() because packages must be added to distros
	 * through \c Distro::append() or \c Distro::operator<<() only.
	 */
	Distro* distro() const;
	
	/** Returns repository the package belongs to.
	 * It is \c 0 when it isn't set.
	 * 
	 * (In fact, a package belongs to a \c Distro, not \c Repository directly.)
	 */
	Repository* repository() const;
	
	/** Guesses package's type and constructs object of corresponding class.
	 * 
	 * Package type is determined by the «Type» attribute.
	 * If «Type» is omitted, CustomPackage class is used.
	 *
	 * Don't confuse it with Package::create(Type, name, section).
	 * That one is for creating new packages while this one is for loading existing packages,
	 * that's why \c section can't be omitted here.
	 */
	static Package* create(const QString& name, ConfigSection* section);
	
	/** Guesses package's type and constructs object of corresponding class.
	 * 
	 * This is overloaded method which should be used when «Name» attribute exists in \c section.attr.
	 * Inside the method, the other version of \c create is called,
	 * and the type-guessing logic remains the same («Type» attribute is used).
	 */
	static Package* create(ConfigSection* section);

	/** Uses first package found in the "_" supersection of the Config (for example, ["_"]["bash"])
	 * and creates a Package object from it.
	 */
	static Package* create(Config* config);
	
	/** Creates Package of type given as constant.
	 */
	static Package* create(const Type& type, const QString& name="Unnamed package", ConfigSection* section=0);
	
	/** Create Package of type given as string.
	 */
	static Package* create(const QString& type, const QString& name="Unnamed package", ConfigSection* section=0);
	
	bool hasErrors() const;
	void setHasErrors(const bool hasErrors);
	
	bool hasWarnings() const;
	void setHasWarnings(const bool hasWarnings);
	
	static QString systemArch();
};

#endif // PACKAGE_H
