#ifndef CONFIG_H
#define CONFIG_H

#include <QtCore/QPair>
#include <QtCore/QVariant>
#include <cutecat/core/macros.h>
#include <cutecat/core/map.h>
#include <cutecat/core/pseudomap.h>

struct ConfigFileEntry {
	QString target;
	QString source;
	bool symlinking;
	ConfigFileEntry(const QString& target="", const QString& source="", const bool symlinking=false);
	bool operator==(const ConfigFileEntry& other) const;
	bool operator!=(const ConfigFileEntry& other) const;
};

struct ConfigFilesSet : public Cutecat::PseudoMap<QString,ConfigFileEntry> {
	QString arch;
	ConfigFilesSet(const QString& arch="");
	virtual QString itemKey(const ConfigFileEntry& item) const;
	virtual ConfigFileEntry* createItem(const QString& key) const;
};

struct ConfigArchsSet : public Cutecat::PseudoMap<QString,ConfigFilesSet> {
	virtual QString itemKey(const ConfigFilesSet& item) const;
	virtual ConfigFilesSet* createItem(const QString& key) const;
	bool containsPath(const QString& path, const QString& arch) const;
};

struct ConfigArchive {
	QString arch;
	QString source;
	ConfigArchive(const QString& arch="", const QString& source="");
	bool operator==(const ConfigArchive& other) const;
	bool operator!=(const ConfigArchive& other) const;
};

struct ConfigArchivesSet : public Cutecat::PseudoMap<QString,ConfigArchive> {
	virtual QString itemKey(const ConfigArchive& item) const;
	virtual ConfigArchive* createItem(const QString& key) const;
};

struct ConfigSection {
	QString name;
	QVariantMap attr;
	ConfigArchsSet files;
	ConfigArchivesSet archives;
	ConfigSection(const QString& name="");
	bool operator==(const ConfigSection& other) const;
	bool operator!=(const ConfigSection& other) const;
};

struct ConfigSuperSection : public Cutecat::PseudoMap<QString,ConfigSection> {
	QString name;
	ConfigSuperSection(const QString& name=QString());
	virtual QString itemKey(const ConfigSection& item) const;
	virtual ConfigSection* createItem(const QString& key) const;
	ConfigSection& firstNamedSection();
};


/*******************************************************/


/** A «low-level» class which parses a config file and saves it back.
 * 
 * \note Currently, save feature is not in use.
 * 
 * The class is in fact a QMap containing modifiable variables.
 * You can use them the following way:
 * \code
 * config["precise main"]["tainer"].attr["Version"] = "0.999";
 * config["precise main"]["tainer"].files["i686"]["/usr/bin/tainer"] = ConfigFileEntry("/home/coolhacker/bin/tainer", false);
 * config["precise main"]["tainer"].archives["i686"] = "/home/coolhacker/tainer.deb";
 * \endcode
 * 
 * (Normally, \c files and \c archives maps aren't filled at the same time:
 * \c files is for CustomPackage only, and \c archives is for ImportedPackage only.)
 * 
 * In fact, Config is not a QMap but a QList, and you can access all data
 * using integer indexes (it is useful when wrapping Config with a QAbstractItemModel):
 * \code
 * config[0].distroname = "precise main";
 * config[0][1].name = "tainer";
 * config[0][1].attr["Version"] = "0.999";
 * config[0][1].files[0].arch = "i686";
 * config[0][1].files[0][0].target = "/usr/bin/tainer";
 * config[0][1].files[0][0].source = "/home/coolhacker/bin/tainer";
 * \endcode
 * When accessing items with string keys, the first item with such "key" will be returned.
 * 
 * Meta-fields before any distribution section are placed into pseudo packages inside a pseudo distribution called "_",
 * for example, config["_"]["package"] or config["_"]["repository"].
 * Please read description of load() for examples of syntax allowed in the files.
 */
class Config : public Cutecat::PseudoMap<QString,ConfigSuperSection>
{
	QString _fileName;
	
public:
	enum Type {Repository, Package};
	
	Config(const QString fileName=QString(), bool loadImmediately=true);
	
	QString fileName() const;
	void setFileName(const QString& fileName);
	
	/** (Re)loads all data from file.
	 * 
	 * The file must have a special syntax similar to normal INI format.
	 * It must have one or more \em sections starting with titles in square brackets.
	 * Every section can contain keys and values in pairs.
	 * For example:
	 * \code
	 * [tainer]
	 * Decription = DEB and APT manager
	 * Version = 0.8
	 * \endcode
	 * 
	 * The most useful part of data — files or archives — is stored using
	 * special syntax with parentheses (not a part of normal INI format).
	 * 
	 * 
	 * Multiline strings (descriptions, scripts, etc.) can be stored using this special syntax:
	 * \code
	 * Description:
	 * 	A long description.
	 * 	It has multiple lines.
	 * 	Every line must start be prepend with a single tabulation.
	 * \endcode
	 * 
	 * 
	 * For CustomPackage, a config section usually should contain lines like these:
	 * \code
	 * Files(every)/usr/share/icons/tainer.png = /home/tainer/tainer-icon.png
	 * Files(every)/usr/share/docs/ = /home/tainer/docs/
	 * Files(i686)/usr/bin/tainer = /home/tainer/build/i686/tainer
	 * Files(x64)/usr/bin/tainer = /home/tainer/build/x64/tainer
	 * \endcode
	 * 
	 * This lines will be read in this configuration:
	 * \code
	 *	- every
	 *  |-- /usr/share/icons/tainer.png => /home/tainer/tainer-icon.png
	 *  |-- /usr/share/docs/ => /home/tainer/docs/
	 *	- i686
	 *  |-- /usr/bin/tainer => /home/tainer/build/i686/tainer
	 *	- x64
	 *  |-- /usr/bin/tainer => /home/tainer/build/x64/tainer
	 * \endcode
	 * 
	 * In this example, the package contains two architectures: \c i686 and \c x64,
	 * and each of them contains its own version of binary \c /usr/bin/tainer.
	 * At the same time, there are platform-independent files which are common for both platforms.
	 * These files are placed into \c every section. One of these files is \c tainer.png,
	 * and the other one is a whole folder \c /usr/share/docs.
	 * This means that all files and folders found in \c /home/tainer/docs at the time of building
	 * will be packed into \c /usr/share/docs in the resulting package.
	 * (This can be used for packing sets of scripts without defining every single script in the config.)
	 * 
	 * Please note that \c every is a magic word meaning "every configuration" while \c all isn't.
	 * There is a real architecture named \c all which is normally used for platform-independent packages.
	 * Files defined with "Files(all)" prefix will be added to the \c all architecture only.
	 * 
	 * There is an additional feature: instead of "=" symbol, you can use "->".
	 * This will mean that, after installation of package, the file will be just a symlink.
	 * Note that in this case the path defined on the right side must be a valid path
	 * on the user's machine, not on the developer's one.
	 * 
	 * The "->" is a CustomPackage-only feature, and Tainer doesn't guarantee
	 * to parse config normally if you use "->" in other sections.
	 * 
	 * 
	 * For ImportedPackage, a config section usually contain lines like these:
	 * \code
	 * Archives(i686) = /home/tainer/build/tainer_0.8_i686.deb
	 * Archives(x86_64) = /home/tainer/build/tainer_0.8_x86_64.deb
	 * \endcode
	 * 
	 * Since ImportedPackage needs only a single file per arhitecture, this syntax
	 * is more simple than the CustomPackage's one.
	 * 
	 * There is no magic word like \c every for ImportedPackage.
	 * 
	 * 
	 * You can include sections (not supersections) from separate files:
	 * \code
	 * [include /path/to/section.tnp]
	 * [include /path/to/directory/*.tnp]
	 * \endcode
	 * The path can be either absolute or relative to the file's location.
	 * With this syntax, every section from "_" supersection of every matched file
	 * will be appended to the current supersection of current config.
	 * 
	 * \note The «include» syntax is now the only feature that \c save() doesn't support.
	 */
	void load();
	
	/** Saves data to given file.
	 */
	void save();
	
	/** Detects and returns config type: Repository (tnr) or Package (tnp).
	* 
	* It is not based on filename, of course.
	* The result is based on existence of "config[_][repository]" section.
	*/
	Type type();
	
	/** Check whether the config contains data for given two-level keys combination.
	 */
	bool contains(const QString& superSectionName, const QString& sectionName);
	
	virtual QString itemKey(const ConfigSuperSection& item) const;
	virtual ConfigSuperSection* createItem(const QString& key) const;
	
	/** Returns \c true if the Config differs from a Config loaded from \c _fileName.
	 *
	 * Unfortunately, there's no way to quickly determine this,
	 * so the method just loads a new Config object and then compares it with the current one.
	 */
	bool isModified();
	
	/** Prints nice output to console for debuggin purposes.
	 */
	void printStructure();
};

#endif // CONFIG_H
