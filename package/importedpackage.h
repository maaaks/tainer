#ifndef IMPORTEDPACKAGE_H
#define IMPORTEDPACKAGE_H

#include <QtCore/QMap>
#include "package.h"

/** A package added to the repository as a ready package file.
 *
 * An imported package doesn't need separate description, changelog and so on:
 * all information of that type is read from the DEB file.
 * ImportedPackage only stores information about:
 *	- its own name (standard Package's name() and setName())
 *  - names of to DEB/RPM/... files for every architecture the package supports
 * 
 * To avoid making unnecessary copies of the package files, they are usually
 * placed inside the Repository's output folder (see Repository::selectFileName()).
 */
class ImportedPackage : public Package
{
	friend class Package;
	ImportedPackage(const QString& name, ConfigSection* confSection);
	
public:
	/** Checks whether packages exist at all entered paths.
	 */
	virtual void checkErrors();
	
	/** For an imported package, «packing» means just copying package to the Repository's folder.
	 */
	virtual void build(const QString& arch, const QString& filePath, Repository* repository = 0);
	
	/** Any file that exists mustn't be rebuilt.
	 */
	virtual bool needToRebuild(const QString& arch, const QString& filePath) const;
	
	/** Returns keys of internal \c _files map.
	 */
	virtual QStringList architectures() const;
	
	QString fileByArchitecture(const QString& architecture);
	
	virtual void clearArchives();
	
	void addArchive(const QString& arch, const QString& archive);
};

#endif // IMPORTEDPACKAGE_H
