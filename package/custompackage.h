#ifndef CUSTOMPACKAGE_H
#define CUSTOMPACKAGE_H

#include <QtCore/QDateTime>
#include <QtCore/QStringList>
#include "package.h"

/** A «custom package» is a package which is made of previously compiled files.
 *
 * It can also be used for packing scripts, documentation or other packages
 * which don't need any compiling but just need to be packed.
 *
 * For DEB, this class uses \c ar and \c tar utilities to create the resulting file.
 */
class CustomPackage : public Package
{
	friend class Package;
	CustomPackage(const QString& name, ConfigSection* confSection);
	
	/** Format string for writing into \c control file.
	 * 
	 * If \c fieldValue is empty, returns nothing. Otherwise, returns something like "Section: graphics".
	 * Every line except the first one will be prefixed with double space.
	 */
	QString formatDebField(const QString& fieldName, const QString& fieldValue);
	
	/** Returns \c true if \c dir or one of its subdirectories
	 * contains a file created or modified at \c date or later.
	 */
	static bool isModifiedAfter(const QString& path, const QDateTime& date);
	
public:
	virtual void build(const QString& arch, const QString& filePath, Repository* repository = 0);
	
	virtual void checkErrors();
	
	/** Returns \c true if file for architecture \c arch at \c filePath
	 * needs to be rebuild because of sources modifications.
	 */
	virtual bool needToRebuild(const QString& arch, const QString& filePath) const;
	
	/** Returns keys of internal \c _files map.
	 */
	virtual QStringList architectures() const;
};

#endif // CUSTOMPACKAGE_H
