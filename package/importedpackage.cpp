#include "importedpackage.h"

#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <cutecat/core/shell.h>
#include "repository.h"

ImportedPackage::ImportedPackage(const QString& name, ConfigSection* confSection) :
	Package(name, confSection)
{
}

void ImportedPackage::checkErrors()
{
	_hasErrors = false;
	_hasWarnings = false;
	foreach (ConfigArchive entry, _confSection->archives) {
		if (entry.source.isEmpty())
			_hasWarnings = true;
		if (entry.arch.isEmpty())
			_hasWarnings = true;
		if (!entry.source.isEmpty() && !QFileInfo(entry.source).exists()) {
			_hasErrors = true;
			return; // An error is the worst that could happen here
		}
	}
}

void ImportedPackage::build(const QString& arch, const QString& filePath, Repository* repository)
{
	runPreBuildScript(arch);
	
	QString source = Cutecat::Shell::expand(_confSection->archives[arch].source, env);
	Cutecat::Shell::forceCopy(source, filePath);
	
	runPostBuildScript(arch);
}

bool ImportedPackage::needToRebuild(const QString& arch, const QString& filePath) const
{
	return false;
}

QStringList ImportedPackage::architectures() const
{
	if (hasProperty("Architectures"))
		return multiProp("Architectures");
	else {
		QStringList archs;
		foreach (ConfigArchive archive, _confSection->archives)
			archs << archive.arch;
		return archs;
	}
}

QString ImportedPackage::fileByArchitecture(const QString& architecture)
{
	if (_confSection->archives.contains(architecture))
		return _confSection->archives[architecture].source;
	else
		return QString();
}

void ImportedPackage::clearArchives()
{
	_confSection->archives.clear();
}

void ImportedPackage::addArchive(const QString& arch, const QString& archive)
{
	_confSection->archives[arch] = archive;
}
