#include "custompackage.h"
#include "repository.h"

#include <QtCore/QDir>
#include <QtCore/QProcess>
#include <QtCore/QTemporaryDir>
#include <QtWidgets/QApplication>
#include <cutecat/core/macros.h>
#include <cutecat/core/shell.h>
#include "definitions.h"
#include "distro.h"
#include "repository.h"
#include "tar.h"

CustomPackage::CustomPackage(const QString& name, ConfigSection* confSection) :
	Package(name, confSection)
{
}

void CustomPackage::checkErrors()
{
	_hasErrors = false;
	_hasWarnings = false;
	foreach (ConfigFilesSet set, _confSection->files)
		foreach (ConfigFileEntry file, set) {
			if (file.source.isEmpty()) {
				_hasWarnings = true;
			}
			else if (!file.symlinking && !QFileInfo(file.source).exists()) {
				_hasErrors = true;
				return; // An error is the worst that could happen here
			}
			else if (file.symlinking && !_confSection->files.containsPath(file.source, set.arch)) {
				_hasWarnings = true;
			}
		}
}


/** Uses Python's \c TarFile module and \c ar archiver to create package with given files.
 * 
 * If \c forceRebuild is \c false, skips building existing package unless it is older than source folders.
 */
void CustomPackage::build(const QString& arch, const QString& filePath, Repository* repository)
{
	// Running pre-build script
	runPreBuildScript(arch);
	
	// Create temporary directory for work
	QTemporaryDir dir(QDir::tempPath() + "/tainer.XXXXXX");
	QDir(dir.path()).mkdir("data");
	QDir(dir.path()).mkdir("control");
	
	/*
	 * CREATING DATA.TAR.GZ
	 */
	QDir dataDir(dir.path()+"/data");
	
	// Choose which files should be packed
	ConfigFilesSet files;
	//  1. Add files that are explicitly set for this architecture
	files << _confSection->files[arch];
	//  2. Add files that are set for MAGICWORD_EVERY architecture
	files << _confSection->files[MAGICWORD_EVERY];
	//  3. If arch is MAGICWORD_CURRENT, add files for current architecture
	if (arch == MAGICWORD_CURRENT)
		files << _confSection->files[systemArch()];
	
	// Iterate through selected files
	foreach (const ConfigFileEntry& entry, files)
	{
		// Expand variables in source
		QString source = Cutecat::Shell::expand(entry.source, env);
		// Remove first slash from target and expand variables, too
		QString target = Cutecat::Shell::expand(entry.target.mid(1), env);
		
		// Create parent directories
		dataDir.mkpath(QFileInfo(target).dir().path());

		// Append file (or symlink to it) into archive
		if (entry.symlinking)
			Cutecat::Shell::shell("ln --symbolic "+source+" "+dataDir.filePath(target));
		else
			Cutecat::Shell::shell("cp --recursive --no-dereference "+source+" "+dataDir.filePath(target));
	}
	
	// Compress data.tar.gz
	QDir::setCurrent(dataDir.path());
	Cutecat::Shell::shell("tar --create --gzip --file=../data.tar.gz *");
	QDir(dataDir.path()).removeRecursively();
	
	
	/*
	 * CREATING CONTROL.TAR.GZ
	 */
	QDir controlDir(dir.path()+"/control");
	
	// Prepare maintainer's name & email (they may fallback to repository's defaults)
	// Name:
	QString maintainerNameFinal = prop("MaintainerName");
	if (repository && maintainerNameFinal.isEmpty())
		maintainerNameFinal = repository->prop("MaintainerName");
	// Email:
	QString maintainerEmailFinal = prop("MaintainerEmail");
	if (repository && maintainerEmailFinal.isEmpty())
		maintainerEmailFinal = repository->prop("MaintainerEmail");
	
	// Write "control" file
	QFile controlFile(dir.path()+"/control/control");
	controlFile.open(QIODevice::WriteOnly);
	QTextStream control(&controlFile);
	control << formatDebField("Package",      name());
	control << formatDebField("Version",      prop("Version", "0.0"));
	control << formatDebField("Section",      prop("Section"));
	control << formatDebField("Architecture", arch);
	control << formatDebField("Maintainer",   maintainerNameFinal + " <" + maintainerEmailFinal + ">");
	control << formatDebField("Homepage",     prop("Homepage"));
	control << formatDebField("Breaks",       prop("Breaks"));
	control << formatDebField("Conflicts",    prop("Conflicts"));
	control << formatDebField("Depends",      prop("Depends"));
	control << formatDebField("Provides",     prop("Provides"));
	control << formatDebField("Recommends",   prop("Recommends"));
	control << formatDebField("Replaces",     prop("Replaces"));
	control << formatDebField("Description",  prop("Summary") + "\n" + prop("Description"));
	controlFile.close();
	
	// Add pre-script and post-script
	WRITE_TO_FILE("preinst",  prop("PreInst"));
	WRITE_TO_FILE("postinst", prop("PostInst"));
	WRITE_TO_FILE("prerm",    prop("PreRm"));
	WRITE_TO_FILE("postrm",   prop("PostRm"));
	
	// Compress data.tar.gz
	QDir::setCurrent(controlDir.path());
	Cutecat::Shell::shell("tar --create --gzip --file=../control.tar.gz *");	
	QDir(controlDir.path()).removeRecursively();
	
	
	/*
	 * Create "debian-binary" file
	 */
	QDir::setCurrent(dir.path());
	Cutecat::Shell::shell("echo 2.0 > debian-binary", dir.path());
	
	
	/*
	 * Finally, create the DEB package
	 */
	QString tempFileName = name() + "_" + prop("Version") + "_" + arch + ".deb";
	QProcess::execute("ar", {"rcs", tempFileName, "debian-binary", "control.tar.gz", "data.tar.gz"});
	
	// Copy it to the place it should be
	Cutecat::Shell::forceCopy(tempFileName, filePath);
	
	// Running post-build script
	runPostBuildScript(arch);
}

bool CustomPackage::isModifiedAfter(const QString& path, const QDateTime& date)
{
	QFileInfo root(path);
	if (root.lastModified() >= date)
		return true;
	else if (!root.isDir())
		// Nothing to iterate
		return false;
	
	QFileInfoList files = root.absoluteDir().entryInfoList(QDir::NoDotAndDotDot);
	
	// First, look for recently modified files & dirs in the first level
	foreach (QFileInfo file, files) {
		if (file.lastModified() >= date)
			return true;
	}
	
	// Then, recursively scan every folder
	foreach (QFileInfo file, files) {
		if (file.isDir())
			if (isModifiedAfter(file.absoluteFilePath(), date))
				return true;
	}
	
	return false;
}

bool CustomPackage::needToRebuild(const QString& arch, const QString& filePath) const
{
	QDateTime date = QFileInfo(filePath).lastModified();
	
	foreach (ConfigFileEntry entry, _confSection->files[arch])
		if (!entry.symlinking)
			if (isModifiedAfter(entry.source, date))
				return true;
	return false;
}

QString CustomPackage::formatDebField(const QString& fieldName, const QString& fieldValue)
{
	if (fieldValue.length() == 0)
		return "";
	else
	{
		// Start with field's name
		QString result = fieldName + ": ";
		
		// Split text into lines and write the first one
		QStringList lines = fieldValue.split("\n");
		result += lines.takeFirst() + "\n";
		
		// Iterate through remaining lines
		foreach (QString line, lines)
		{
			// Empty line? Write a space plus a period.
			if (line.trimmed().length() == 0)
				result += " .\n";
			// Non-empty line? Start it with a space.
			else
				result += " " + line + "\n";
		}
		
		// Return result
		return result;
	}
}

QStringList CustomPackage::architectures() const
{
	if (hasProperty("Architectures"))
		return multiProp("Architectures");
	else {
		QStringList archs;
		foreach (ConfigFilesSet filesSet, _confSection->files)
			if (filesSet.arch!=MAGICWORD_EVERY && filesSet.arch!=MAGICWORD_CURRENT)
				archs << filesSet.arch;
		return archs;
	}
}
