# Tainer #
## Build Debian packages with ease! ##

**Tainer** is an easy-to-use command-line utility for building DEB packages and whole APT repositories from a simple INI-style configuration files. It was created for those who don't build packages every day and do not want to remember all those dpkg utilities' names. Tainer's configuration syntax is very intuitive when reading and even almost intuitive when writing. You just define some meta information and files to be packed, and Tainer will do all routine like creating temporary directories and packing files into archives.

### Documentation ###

Please read [full documentation on how to use Tainer](https://maaaks.me/tainer/manual.html).

You can also look at simple examples of using Tainer:

 * [Packaging a simple script](https://maaaks.me/tainer/examples/script.html)
 * [Packaging NetBeans](https://maaaks.me/tainer/examples/netbeans.html)
 * [Packaging QtMind](https://maaaks.me/tainer/examples/qtmind.html)
 * [Maintaing a repository](https://maaaks.me/tainer/examples/repository.html)