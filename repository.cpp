#include "repository.h"

#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QProcess>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>
#include <cutecat/core/shell.h>
#include "definitions.h"
#include "distro.h"
#include "package.h"
#include "package/importedpackage.h"
#include "package/custompackage.h"

using namespace Cutecat;

Repository::Repository(Config*const config)
{
	if (config) {
		_fileName = config->fileName();
		loadConfig(config);
	}
}

void Repository::loadConfig(Config*const config)
{
	// Forget all previously loaded package info
	clear();
	
	// Store pointer to the whole Config
	_config = config;
	_properties = &(*config)["_"]["repository"].attr;
	
	// Load all packages (ignore "_" supersection)
	int configSize = config->count();
	for (int ss=0; ss<configSize; ss++) {
		ConfigSuperSection& superSection = (*config)[ss];
		
		if (superSection.name != "_") {
			// Add new distro to the list
			Distro* distro = new Distro(&superSection);
			append(distro, true);
			
			int superSectionSize = superSection.count();
			for (int s=0; s<superSectionSize; s++) {
				ConfigSection& section = superSection[s];
				
				// Read new package's settings from ConfigSection and add it to distro
				Package* package = Package::create(&section);
				distro->append(package, true);
			}
		}
	}
}

void Repository::saveConfig()
{
	// Save config to file
	_config->setFileName(_fileName);
	_config->save();
}

QString Repository::fileName() const
{
	return _fileName;
}

void Repository::setFileName(const QString& fileName)
{
	_fileName = fileName;
}

Config* Repository::config()
{
	return _config;
}

QString Repository::prop(const QString& key, const QString& defaultValue) const
{
	if (_properties)
		return (*_properties).value(key, defaultValue).toString();
	else
		return defaultValue;
}

bool Repository::hasProperty(const QString& key)
{
	return _properties->contains(key);
}

void Repository::setProperty(const QString& key, const QString& value)
{
	if (!_properties)
		_properties = new QVariantMap;
	(*_properties)[key] = value;
}

QString Repository::selectFileName(const QString& buildPath, const Package* package, const QString& arch)
{
	QString name = package->name();
	QChar n = name.startsWith("lib") ? name[3] : name[0];
	QString version = package->prop("Version", "0.0");
	
	QString filePath = buildPath + "/pool/" + n + "/" + name + "_" + version + "_" + arch + ".deb";
	return filePath;
}

void Repository::build(QString path, QStringList archs, bool forceRebuild, BuildMode mode)
{
	// Choose output path
	if (path.isEmpty())
		path = prop("BuildPath", QDir().absolutePath());
	QDir::setCurrent(path);
	
	// Choose architectures
	if (archs.isEmpty())
		archs = architectures();
	
	// This style will be used for console output
	Shell::Style style(Shell::Fg::Purple, Shell::Bg::NoChange, Shell::Font::Bold);
	
	Shell::println("Building \"" + prop("Title") + "\" (" + archs.join(", ") + ")...", style);
	
	// First of all, repack all the packages for all the architectures in all thedistributions
	foreach (Distro* distro, *this) {
		Shell::println("  Distribution: " + distro->name(), style);
		bool somethingWasBuilt = false;
		
		foreach (Package* package, *distro) {
			foreach (QString arch, package->architectures()) {
				if (archs.contains(arch)
				|| (archs.contains(MAGICWORD_CURRENT) && arch==Package::systemArch())
				|| arch == MAGICWORD_EVERY) {
					// Select file for building
					QString fileName = selectFileName(path, package, arch);
					
					// If file exists there, check if needs to be rebuilt
					if (QFileInfo(fileName).exists() && !forceRebuild && !package->needToRebuild(arch, fileName))
						continue;
					
					// Build the package
					Shell::println("    Building " + package->name() + " (" + arch + ")...", style);
					package->build(arch, fileName);
					somethingWasBuilt = true;
				}
			}
		}
		
		// Generate "Packages" indexes for every architecture in current distribution
		if (mode==ReindexOnly || (mode!=BuildOnly && somethingWasBuilt))
		{
			Shell::println("    Generating indexes...", style);
			foreach (QString arch, archs)
			{
				// Create directory for "Packages*" files and enter it
				QDir::setCurrent(path);
				QString curDistroPath = "dists/" + distro->name().replace(" ","/") + "/binary-" + arch;
				QProcess::execute("mkdir", {"--parents", curDistroPath});
				
				// Create "Packages" file with dpkg-scanpackages
				QString scanPackagesCommand = QString("dpkg-scanpackages --arch %1 pool > %2/Packages 2>/dev/null").arg(arch, curDistroPath);
				Shell::shell(scanPackagesCommand);
				
				// Create gzipped and bzipped versions of "Packages" file
				Shell::shell(QString("cat %1/Packages | gzip  --stdout --best > %1/Packages.gz ").arg(curDistroPath));
				Shell::shell(QString("cat %1/Packages | bzip2 --stdout --best > %1/Packages.bz2").arg(curDistroPath));
			}
		}
	}
	
	Shell::println("Finished.", style);
}

QStringList Repository::architectures() const
{
	QStringList archs;
	
	foreach (Distro *distro, *this)
		foreach (Package *package, *distro)
			foreach (QString arch, package->architectures())
				if (!archs.contains(arch))
					archs << arch;
	
	return archs;
}

void Repository::printStructure() const
{
	foreach (Distro* distro, *this) {
		OUT(" * " + distro->name());
		foreach (Package* package, *distro)
			OUT(" |-- " + package->name());
	}
}

Package* Repository::findPackage(const QString& name) const
{
	foreach (Distro*const distro, *this) {
		foreach (Package*const package, *distro) {
			const QString shortName = package->name();
			const QString fullName = distro->name() + "/" + package->name();
			if (shortName == name || fullName == name)
				return package;
		}
	}
	return 0;
}

void Repository::append(Distro* distro, bool alreadyInConfig)
{
	QList<Distro*>::append(distro);
	distro->_repository = this;
	if (!alreadyInConfig)
		*_config << distro->confSuperSection();
}

Repository& Repository::operator<<(Distro* distro)
{
	append(distro);
	return *this;
}
