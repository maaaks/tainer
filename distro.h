#ifndef DISTRO_H
#define DISTRO_H

#include <QtCore/QList>
#include <QtCore/QString>
#include <cutecat/core/shell.h>
#include "config.h"

class Package;
class Repository;

class Distro : public QList<Package*>
{
	friend class Repository; // Repository needs access to Distro::_repository pointer
	
	Repository* _repository = 0;
	ConfigSuperSection* _confSuperSection = 0;
	
public:
	Distro(QString name="");
	Distro(ConfigSuperSection* confSuperSection);
	
	Repository* repository() const;
	ConfigSuperSection* confSuperSection() const;
	
	QString name() const;
	void setName(const QString& name);
	
	void reindex(QString repoPath, QStringList archs, Cutecat::Shell::Style style);
	
	void append(Package* package, bool alreadyInConfig=false);
	Distro& operator<<(Package* package);
};

#endif // DISTRO_H
