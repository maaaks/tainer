#include "package.h"

#include <QtCore/QTemporaryDir>
#include <QtCore/QTemporaryFile>
#include <cutecat/core/shell.h>
#include <stdexcept>
#include "definitions.h"
#include "distro.h"
#include "package/importedpackage.h"
#include "package/custompackage.h"
#include "repository.h"

Package::Package(const QString& name, ConfigSection* confSection) :
	env(QProcessEnvironment::systemEnvironment())
{
	_confSection = confSection ? confSection : new ConfigSection;
	_confSection->name = name;
}

// Package::Package(ConfigSection* confSection)
// {
// 	_confSection = confSection ? confSection : new ConfigSection;
// }

Package::~Package()
{
}

QString Package::systemArch()
{
	static QString result;
	
	if (result.isNull()) {
		QProcess uname;
		uname.start("uname", {"--processor"});
		uname.waitForFinished(-1);
		result = uname.readAllStandardOutput().trimmed();
	}
	return result;
}

ConfigSection* Package::confSection() const
{
	return _confSection;
}

QString Package::prop(const QString& key, const QString& defaultValue) const
{
	QVariant variant = _confSection->attr.value(key, defaultValue);
	if (variant.type() == QVariant::StringList)
		return variant.toStringList().join(", ");
	else
		return variant.toString();
}

QStringList Package::multiProp(const QString& key) const
{
	QVariant variant = _confSection->attr.value(key);
	if (variant.type() == QVariant::StringList)
		return variant.toStringList();
	else {
		QStringList items = variant.toString().split(',');
		for (int i=0; i<items.count(); i++)
			items[i] = items[i].trimmed();
		return items;
	}
}

bool Package::hasProperty(const QString& key) const
{
	return _confSection->attr.contains(key);
}

void Package::setProperty(const QString& key, const QString& value)
{
	_confSection->attr[key] = value;
}

void Package::setProperty(const QString& key, const QStringList& value)
{
	_confSection->attr[key] = value;
}

QString Package::name() const
{
	return _confSection->name;
}

void Package::setName(const QString &name)
{
	_confSection->name = name;
}

Package* Package::create(const QString& name, ConfigSection* section)
{
	QString type = section->attr.value("Type").toString();
	return create(type, name, section);
}

Package* Package::create(ConfigSection* section)
{
	QString type = section->attr.value("Type").toString();
	return create(type, section->name, section);
}

Package* Package::create(Config *config)
{
	return Package::create(&(*config)["_"].firstNamedSection());
}

Package* Package::create(const Package::Type& type, const QString& name, ConfigSection* section)
{
	Package* package;
	switch (type)
	{
		case Custom:
			package = new CustomPackage(name, section);
			break;
			
		case Imported:
			package = new ImportedPackage(name, section);
			break;
			
		default:
			QString error = QString("Unknown package type: ") + type;
			OUT(error);
			throw new std::logic_error(error.toStdString());
	}
	
	package->checkErrors();
	return package;
}

Package* Package::create(const QString& type, const QString& name, ConfigSection* section)
{
	if (type == "CustomPackage" || type.isEmpty())
		return Package::create(Package::Custom, name, section);
	else if (type == "ImportedPackage")
		return Package::create(Package::Imported, name, section);
	else {
		QString error = QString("Unknown package type: ") + type;
		OUT(error);
		throw new std::logic_error(error.toStdString());
	}
}

void Package::build(const QString& arch, const QDir& dir)
{
	QString postfix = (arch==MAGICWORD_CURRENT) ? systemArch() : arch;
	QString fileName = name() + "_" + prop("Version") + "_" + postfix + ".deb";
	build(arch, dir.absoluteFilePath(fileName));
}

void Package::saveAs(const QString& fileName)
{
	Config config(fileName, false);
	config["_"] << _confSection;
	config.save();
}

Distro* Package::distro() const
{
	return _distro;
}

Repository* Package::repository() const
{
	return _distro ? _distro->repository() : 0;
}

bool Package::hasErrors() const
{
	return _hasErrors;
}

void Package::setHasErrors(const bool hasErrors)
{
	_hasErrors = hasErrors;
}

bool Package::hasWarnings() const
{
	return _hasWarnings;
}

void Package::setHasWarnings(const bool hasWarnings)
{
	_hasWarnings = hasWarnings;
}


void Package::runPreBuildScript(const QString& arch)
{
	// If the package doesn't have PreBuild script, do nothing
	if (!hasProperty("PreBuild"))
		return;
	
	// Where to create directories?
	Repository*const repo = repository();
	QDir cacheDir;
	if (hasProperty("CacheDir"))
		cacheDir = prop("CacheDir");
	else if (repo && repo->hasProperty("CacheDir"))
		cacheDir = repo->prop("CacheDir") + "/" + name();
	else if (repo && env.contains("TAINER_CACHEDIR_" + repo->prop("Title")))
		cacheDir = env.value("TAINER_CACHEDIR_" + repo->prop("Title")) + "/" + name();
	else if (env.contains("TAINER_CACHEDIR"))
		cacheDir = env.value("TAINER_CACHEDIR") + "/" + name();
	else {
		QTemporaryDir tempDir("tainerCache.XXXXXX");
		tempDir.setAutoRemove(false);
		cacheDir = tempDir.path();
	}
	
	// Create directories
	cacheDir.mkpath("sources");
	cacheDir.mkpath("build");
	
	// Create environment
	env.insert("SRC", cacheDir.absoluteFilePath("sources"));
	env.insert("BUILD", cacheDir.absoluteFilePath("build"));
	env.insert("ARCH", arch==MAGICWORD_CURRENT ? systemArch() : arch);
	
	// Create temporary file where the script can write variables
	QTemporaryFile tempFile;
	tempFile.open();
	tempFile.close();
	
	// Run the script with the environment in the $SRC directory
	Cutecat::Shell::runScript(prop("PreBuild"), {tempFile.fileName()}, env.value("SRC"), env);
	
	// Read the variables set by script
	tempFile.open();
	while (tempFile.bytesAvailable() > 0) {
		QString line = tempFile.readLine().trimmed();
		QString name = line.section('=', 0, 0);
		QString value = line.section('=', 1, -1);
		env.insert(name, value);
	}
	tempFile.close();
}


void Package::runPostBuildScript(const QString& arch)
{
	// If the package doesn't have PostBuild script, do nothing
	if (!hasProperty("PostBuild"))
		return;
	
	// Just run the script using environment from PreBuild
	Cutecat::Shell::runScript(prop("PostBuild"), {}, env.value("SRC"), env);
}
