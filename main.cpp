#include <cutecat/core/macros.h>
#include <stdexcept>

#include "config.h"
#include "definitions.h"
#include "distro.h"
#include "package.h"
#include "repository.h"

enum class Action {
	Build,
	BuildNoReindex,
	List,
	Reindex,
	Help,
};

struct Options {
	Action action = Action::Build;
	QString file;
	QStringList archs;
	QString package;
	bool force = false;
	QString output = QDir::currentPath(); // current directory by default
};

/**
 * Parsing arguments and return them in a \c Options structure.
 */
Options parse(int argc, char **argv)
{
	// Initialize map for converting string options to Actions.
	// TODO When Qt 5.1 arrives to Ubuntu, construct QMap with std::initializer_list,
	// see http://qt-project.org/doc/qt-5/qmap.html#QMap-2 . It's more readable.
	QMap<QString,Action> StringToAction;
	StringToAction["build"]           = Action::Build;
	StringToAction["build-noreindex"] = Action::BuildNoReindex;
	StringToAction["list"]            = Action::List;
	StringToAction["reindex"]         = Action::Reindex;
	StringToAction["-help"]           = Action::Help;
	StringToAction["--help"]          = Action::Help;
	StringToAction["-h"]              = Action::Help;
	StringToAction["help"]            = Action::Help;
	
	Options o;
	for (int i=1; i<argc; i++)
	{
		QString arg = argv[i];
		
		if (i==1 && StringToAction.contains(arg)) {
			o.action = StringToAction[arg];
		}
		
		else if (arg == "--force") {
			o.force = true;
		}
		
		else if (arg == "--output") {
			o.output = argv[++i];
		}
		else if (arg.startsWith("--output=")) {
			o.output = arg.mid(9);
		}
		
		else if (arg == "--arch") {
			o.archs = QString(argv[++i]).split(',');
		}
		else if (arg.startsWith("--arch=")) {
			o.archs = arg.mid(7).split(',');
		}
		
		else if (arg == "--package") {
			o.package = argv[++i];
		}
		else if (arg.startsWith("--package=")) {
			o.package = arg.mid(10);
		}
		
		else {
			o.file = arg;
		}
	}
	
	return o;
}


void buildOrReindexRepository(Repository* repository, Action action, QStringList archs, QString output, bool force)
{
	switch (action) {
	case Action::Build:
		repository->build(output, archs, force, Repository::BuildAndReindex);
		break;
	case Action::BuildNoReindex:
		repository->build(output, archs, force, Repository::BuildOnly);
		break;
	case Action::Reindex:
		repository->build(output, archs, force, Repository::ReindexOnly);
	default:
		;
	}
}


/** The main function. Creates window and optionally loads a repository.
 */
int main(int argc, char **argv)
{
	Options o = parse(argc, argv);
	
	if (o.action == Action::Help) {
		OUT(
			"Tainer " TAINER_APP_VERSION "\n"
			"Package and repository creator for Debian-based systems. \n"
			"\n"
			"Usage: tainer [build|build-noreindex|reindex|list] FILE [--force] [--arch=ARCH] [--output=OUTPUT] [--packages=PACKAGES]\n"
		);
	}
	
	
	////////////////////////////////////
	
	
	if (o.file.isNull())
		DIE("File not given.\n");
	
	// Load config file
	Config* config = new Config(o.file);
	
	// Load the object to work with: either package or repository
	Package* package = 0;
	Repository* repository = 0;
	if (config->type() == Config::Package) {
		package = Package::create(config);
	} else if (config->type() == Config::Repository && !o.package.isEmpty()) {
		repository = new Repository(config);
		package = repository->findPackage(o.package);
		if (package == 0)
			DIE("Package '"+o.package+"' not found in the repository.");
	} else {
		repository = new Repository(config);
	}
	
	// Now work with package (if an exact package is loaded) or work with repository
	if (package != 0)
	{
		if (o.action == Action::Build)
		{
			if (o.archs.length() > 0)
			{
				// If only one architecture is given, use --output as filename.
				// Else, use --output as folder for standard-named files.
				if (o.archs.length() == 1)
					package->build(o.archs[0], o.output);
				else
					foreach(QString arch, o.archs)
						package->build(arch, o.output);
			}
			else
			{
				// If --arch argument is not set, build package for all available architectures
				// and use --output as folder for standard-named files
				foreach (QString arch, package->architectures())
					package->build(arch, o.output);
			}
		}
	}
	else
	{
		// It's a repository
		Repository* repository = new Repository(config);
		
		if (o.action == Action::Build || o.action == Action::BuildNoReindex || o.action == Action::Reindex)
		{
			// Choose what architectures to build.
			// By default, build all architectures.
			QStringList architectures = o.archs.isEmpty()
				? repository->architectures()
				: o.archs;
			
			// Build (apply --force argument if given)
			if (o.action == Action::Build)
				repository->build(o.output, architectures, o.force, Repository::BuildAndReindex);
			if (o.action == Action::BuildNoReindex)
				repository->build(o.output, architectures, o.force, Repository::BuildOnly);
			if (o.action == Action::Reindex)
				repository->build(o.output, architectures, o.force, Repository::ReindexOnly);
		}
		else if (o.action == Action::List)
		{
			// Print structure of the repository
			repository->printStructure();
		}
	}
}
