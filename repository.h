#ifndef REPOSITORY_H
#define REPOSITORY_H

#include <QtCore/QDir>
#include <QtCore/QSet>
#include <QtCore/QSettings>
#include "config.h"
#include "cutecat/core/cutesettings.h"
#include "cutecat/core/shell.h"

class Package;
class Distro;

/** Represents an abstract repository project.
 *
 * A repository project consists of:
 *	- metadata: description, maintainer's email, ...
 *	- configuration: publishing settings, GPG, ...
 *	- list of packages or way to auto-detect it
 */
class Repository : public QList<Distro*>
{
public:
	enum BuildMode {
		BuildAndReindex,
		BuildOnly,
		ReindexOnly,
	};
	
protected:
	QString _fileName;
	Config* _config;
	QVariantMap* _properties = 0;
	
public:
	/** Loads a Repository saved to the given file.
	 *
	 * If the file doesn't exist yet, it creates a new empty repo there.
	 *
	 * @param fileName Full path to the ini-file
	 */
	Repository(Config*const config=0);
	
	QString prop(const QString& key, const QString& defaultValue="") const;
	bool hasProperty(const QString& key);
	void setProperty(const QString& key, const QString& value);
	
	QString fileName() const;
	void setFileName(const QString& fileName);
	
	Config* config();
	
	/** Converts original filename of a package into a full path for placing the file.
	 * 
	 * For example, in a DEB repository the result for "abc.deb" may be "pool/abc.deb" or "pool/a/abc.deb".
	 * The result can be used as an argument to archiver tools.
	 */
	QString selectFileName(const QString& buildPath, const Package* package, const QString& arch);
	
	/** Builds all packages and creates all necessary files.
	 * 
	 * This includes calling Package::build() for every architecture in \c archs,
	 * and then executing \c dpkg-scanpackages for every architecture.
	 * If \c is not given, repository is built for all available architecture
	 * (this means, for all architectures available for packages).
	 * 
	 * By default, the repository is built in its default filepath.
	 * It can be forced to be built in another place (when using CLI).
	 * 
	 * Unless \c forceRebuild is \c true, packages whose sources haven't updated
	 * since last build won't be rebuilded again.
	 */
	void build(QString path="", QStringList archs=QStringList(), bool forceRebuild=false, BuildMode mode=BuildAndReindex);
	
	/** Loads config from the already initialized Config object.
	 * 
	 * Since Repository is intended to be a proxy which stores and edits data directly in Config,
	 * loadConfig() accepts a pointer to Config and passes pointers to its sections and supersections
	 * to all objects inside the repository (namely, Distros and Packages).
	 */
	void loadConfig(Config*const config);
	
	/** Saves config back to file.
	 * 
	 * This doesn't do anything except calling Config's \c setFileName() and \c save() methods
	 * because all the data is already edited inside Config.
	 */
	void saveConfig();
	
	/** Finds and returns all architectures that are mentioned in the repository's packages.
	 * 
	 * Useful for build().
	 */
	QStringList architectures() const;
	
	/** Outputs the repository's structure to console, for debugging purposes.
	 */
	void printStructure() const;
	
	/** Finds and returns package with given name. If not found, returns 0.
	 * 
	 * If you have package «xyz» in both «distro1» and «distro2» and you want to make sure
	 * that this method will return the one from from «distro1», give «distro1/xyz» as the name.
	 */
	Package* findPackage(const QString& name) const;
	
	void append(Distro* distro, bool alreadyInConfig=false);
	Repository& operator<<(Distro* distro);
};

#endif // REPOSITORY_H
