#include "distro.h"

#include "package.h"

using namespace Cutecat;

Distro::Distro(QString name)
{
	_confSuperSection = new ConfigSuperSection;
	_confSuperSection->name = name;
}

Distro::Distro(ConfigSuperSection *confSuperSection)
{
	_confSuperSection = confSuperSection;
}

Repository* Distro::repository() const
{
	return _repository;
}

ConfigSuperSection* Distro::confSuperSection() const
{
	return _confSuperSection;
}

QString Distro::name() const
{
	return _confSuperSection->name;
}

void Distro::setName(const QString& name)
{
	_confSuperSection->name = name;
}

void Distro::reindex(QString repoPath, QStringList archs, Shell::Style style)
{
	// TODO: implement Distro::reindex()
}

void Distro::append(Package* package, bool alreadyInConfig)
{
	QList<Package*>::append(package);
	package->_distro = this;
	if (!alreadyInConfig)
		*_confSuperSection << package->confSection();
}

Distro& Distro::operator<<(Package* package)
{
	append(package);
	return *this;
}
