project(Tainer)

# CMake
cmake_minimum_required(VERSION 2.8.9)
set(CMAKE_VERBOSE_MAKEFILE 0)

# C++
add_definitions(-std=c++11 -fPIC -Wno-comment)

# Qt
find_package(Qt5Widgets)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

# Sources
file(GLOB_RECURSE H "*.h")
file(GLOB_RECURSE CPP "*.cpp")
file(GLOB_RECURSE UI "*.ui")
file(GLOB_RECURSE QRC "*.qrc")

qt5_wrap_ui(UI ${UI})
qt5_add_resources(QRC_OUT ${QRC})

add_executable(tainer main.cpp ${H} ${CPP} ${UI} ${QRC_OUT})
target_link_libraries(tainer cutecat-core python2.7)
qt5_use_modules(tainer Core Network)

install(TARGETS tainer RUNTIME DESTINATION bin)

# Uninstall all the files (code copied from http://www.vtk.org/Wiki/CMake_FAQ)
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)
add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
