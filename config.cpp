#include "config.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QRegExp>
#include <QtCore/QSet>
#include <QtCore/QStringList>
#include <glob.h>
#include <stdexcept>
#include <unistd.h>
#include <cutecat/core/macros.h>
#include "definitions.h"

Config::Config(const QString fileName, bool loadImmediately)
{
	// Create "_" pseudo distribution (to guarantee that it exists)
	(*this) << new ConfigSuperSection("_");
	_fileName = fileName;

	// Load data from file
	if (loadImmediately && !_fileName.isEmpty())
		load();
}

ConfigFileEntry::ConfigFileEntry(const QString& target, const QString& source, const bool symlinking) :
	target(target), source(source), symlinking(symlinking) {}
ConfigFilesSet::ConfigFilesSet(const QString& arch) :
	arch(arch) {}
ConfigArchive::ConfigArchive(const QString& arch, const QString& source) :
	arch(arch), source(source) {}
ConfigSection::ConfigSection(const QString& name) :
	name(name) {}
ConfigSuperSection::ConfigSuperSection(const QString& name) :
	name(name) {}

QString Config::            itemKey(const ConfigSuperSection& item) const { return item.name;   }
QString ConfigSuperSection::itemKey(const ConfigSection& item)      const { return item.name;   }
QString ConfigArchsSet::    itemKey(const ConfigFilesSet& item)     const { return item.arch;   }
QString ConfigArchivesSet:: itemKey(const ConfigArchive& item)      const { return item.arch;   }
QString ConfigFilesSet::    itemKey(const ConfigFileEntry& item)    const { return item.target; }

ConfigSuperSection* Config::            createItem(const QString &key) const { return new ConfigSuperSection(key); }
ConfigSection*      ConfigSuperSection::createItem(const QString &key) const { return new ConfigSection(key);      }
ConfigFilesSet*     ConfigArchsSet::    createItem(const QString &key) const { return new ConfigFilesSet(key);     }
ConfigArchive*      ConfigArchivesSet:: createItem(const QString &key) const { return new ConfigArchive(key);      }
ConfigFileEntry*    ConfigFilesSet::    createItem(const QString &key) const { return new ConfigFileEntry(key);    }


////////////////////////////////////////


/** Checks whether given file \c path seems to be available for given \c arch (or «every» arch).
 * If current arch is «every», checks whether given file \c path is available in every other arch.
 * 
 * The method checks not only entered paths but also files under them in the real filesystem.
 */
bool ConfigArchsSet::containsPath(const QString& path, const QString& arch) const
{
	// If current arch is «every», run the function for each other arch
	if (arch == MAGICWORD_EVERY) {
		foreach (const ConfigFilesSet& files, *this)
			if (arch != MAGICWORD_EVERY && !containsPath(path, files.arch))
				return false;
		return true;
	}
	
	const QStringList pathParts = path.split("/", QString::SkipEmptyParts);
	
	// Iterate through files of the named architecture
	if (contains(arch)) {
		foreach (const ConfigFileEntry& file, (*this)[arch])
		{
			// If file.target is completely equal to path we're looking for, return success
			if (path == file.target) {
				return true;
			}
			// If file.target seems is a higher-level directory of path, we need to go deeper
			else if (!file.target.isEmpty()) {
				const QStringList fileTargetParts = file.target.split("/", QString::SkipEmptyParts);
				
				if (pathParts.mid(0, fileTargetParts.length()) == fileTargetParts)
				{
					const QString relativePath = QStringList( pathParts.mid(fileTargetParts.length()) ).join("/");
					if (!file.symlinking) {
						if (QFile::exists(file.source+"/"+relativePath))
							return true;
					} else {
						if (containsPath(file.source+"/"+relativePath, arch))
							return true;
					}
				}
			}
		}
	}
	
	// Check «every» architecture, too
	if (arch != "any" && containsPath(path, "any"))
		return true;
	
	return false;
}

////////////////////////////////////////


/** Compares two config sections.
 *
 * This includes smart comparing of \c attr map: empty fields are ignored when comparing.
 */
bool ConfigSection::operator==(const ConfigSection& other) const
{
	if (name != other.name)
		return false;
	if (files != other.files)
		return false;
	if (archives != other.archives)
		return false;
	
	// Now we are going to compare set of attributes.
	// First of all, get QSet of all keys existing in any of the two lists.
	QSet<QString> allKeys = QSet<QString>::fromList(attr.keys() + other.attr.keys());
	
	// Start iterating them
	foreach (QString key, allKeys) {
		QVariant v1 = attr[key];
		QVariant v2 = other.attr[key];
		
		// A value is empty if it either is empty (empty string/empty string list)
		// or doesn't exist (QMap's operator[]() returns invalid QVariant for unexistent keys)
		bool isV1Empty = !v1.isValid() || (
			(v1.type()==QVariant::String && v1.toString().isEmpty())
			|| (v1.type()==QVariant::String && v1.toStringList().isEmpty())
		);
		bool isV2Empty = !v2.isValid() || (
			(v2.type()==QVariant::String && v2.toString().isEmpty())
			|| (v2.type()==QVariant::String && v2.toStringList().isEmpty())
		);
		
		// When both values are empty, they are considered equal
		// even if one of them didn't exist, in fact
		if (isV1Empty && isV2Empty)
			continue;
		
		// Finally, compare values. If they differ, the whole ConfigSection differs.
		if (v1 != v2)
			return false;
	}
	
	return true;
}

bool ConfigSection::operator!=(const ConfigSection& other) const
{
	return !operator==(other);
}


////////////////////////////////////////


bool ConfigArchive::operator==(const ConfigArchive& other) const
{
	return arch == other.arch
		&& source == other.source;
}

bool ConfigArchive::operator!=(const ConfigArchive& other) const
{
	return !operator==(other);
}


////////////////////////////////////////


bool ConfigFileEntry::operator==(const ConfigFileEntry& other) const
{
	return target == other.target
		&& source == other.source
		&& symlinking == other.symlinking;
}

bool ConfigFileEntry::operator!=(const ConfigFileEntry& other) const
{
	return !operator==(other);
}


////////////////////////////////////////


/** Returns first section inside "_" supersection that is named other than "_".
 *
 * Throws an exception if no such section exists.
 */
ConfigSection& ConfigSuperSection::firstNamedSection()
{
	for (int i=0; i<length(); i++)
		if ((*this)[i].name != "_")
			return (*this)[i];
	throw new std::logic_error("Can't find named section.");
}


////////////////////////////////////////


QString Config::fileName() const
{
	return _fileName;
}

void Config::setFileName(const QString& fileName)
{
	_fileName = fileName;
}

void Config::load()
{
	// Open file
	QFile in(_fileName);
	if (in.exists() && !in.open(QIODevice::ReadOnly))
		throw new std::logic_error("Cannot open file for reading");
	
	// Clear old data
	clear();
	
	// Prepare five regular expressions.
	// The first is for supersection titles
	QRegExp reSuperTitle(
		"\\s*\\[\\[\\s*"   // double opening bracket with optional spaces
		"(\\S+|\\S.+\\S)"  // supersection title (can contain spaces inside)
		"\\s*\\]\\]\\s*"   // closing bracket with optional spaces
	);
	
	// The second is for section titles
	QRegExp reTitle(
		"\\s*\\[\\s*"  // opening bracket with optional spaces
		"(\\S+)"       // section title (cannot contain spaces!)
		"\\s*\\]\\s*"  // closing bracket with optional spaces
	);
	
	// The third is for key-value pairs
	// with optional "Files(i686)"-style structures
	// or "Archive(i686)"-style structures
	QRegExp reAttr(
		"\\s*"                    // optional spaces
		"(?:"
			"Archives"            // "Archives"
			"\\(([^)]+)\\)"       // "(i686)"       [1]
			"|"
			"(?:"
				"Files"           // "Files"
				"\\(([^)]+)\\)"   // "(i686)"       [2]
			")?"
			"([\\S^=]+)"          // key            [3]
		")"
		"\\s*(=|->)\\s*"          // "=" or "->"    [4]
		"(.*)"                    // value          [5]
	);
	
	// The fourth is for multiline key-value pairs
	QRegExp reMultilineAttr(
		"\\s*"       // optional spaces
		"([\\S^=]+)" // key
		"\\s*:\\s*"  // ":"
	);
	
	// The fifth is for including other files
	QRegExp reInclude(
		"\\s*\\[\\s*"     // opening bracket with optional spaces
		"include\\s+"     // magicword and spaces
		"(\\S+|\\S.+\\S)"  // path to file (can contain spaces inside)
		"\\s*\\]\\s*"     // closing bracket with optional spaces
	);
	
	// Prepare for parsing
	ConfigSuperSection* superSection = new ConfigSuperSection("_"); // Meta supersection
	ConfigSection* section = new ConfigSection("_"); // Meta section
	QString multilineKey; // Place to store key while reading multiline value
	
	// Initialize config with meta supersection and meta section
	*this << superSection;
	*superSection << section;
	
	// Start iterating through lines!
	while (!in.atEnd()) {
		QString line = in.readLine();
		
		if (!multilineKey.isNull()) {
			// This is a line after a «MultilineKey:» line.
			// If it starts with tabulation, append it to the multiline value.
			// Otherwise, switch back to normal parsing mode.
			if (line.startsWith("\t")) {
				QString oldValue = section->attr[multilineKey].toString();
				section->attr[multilineKey] = oldValue + line.mid(1);
				continue;
			} else {
				multilineKey = QString();
			}
		}
		
		if (reSuperTitle.exactMatch(line)) {
			// This is a [[supertitle]] which starts a new supersection
			superSection = new ConfigSuperSection( reSuperTitle.cap(1) );
			*this << superSection;
		}
		else if (reTitle.exactMatch(line)) {
			// This is a [title] which starts a new section
			section = new ConfigSection( reTitle.cap(1) );
			*superSection << section;
		}
		else if (reAttr.exactMatch(line)) {
			// This is one of following types of lines:
			//  key = value
			//  Files(arch)key = value
			//  Files(arch)key -> value
			//  Archives(arch) = value
			
			// Copy captured texts to named variables for further readability
			QString archiveArch = reAttr.cap(1).trimmed();
			QString fileArch    = reAttr.cap(2).trimmed();
			QString key         = reAttr.cap(3).trimmed();
			bool    symlinking  = reAttr.cap(4) == "->";
			QString value       = reAttr.cap(5).trimmed();
			
			// Every value in config is a structure containing .attr and .files
			// where .attr is a simple QString->QString map,
			// and .files is a complex map of maps of maps of values.
			// In the second case, we must create unexistent submaps of all levels
			// before assigning the value.
			// In addition, a value in the second case is not just a string, but
			// a structure containing a string (path) and a boolean value (symlinking)
			// which must be set to true when "->" is used instead of "=".
			
			// This all may look a sort of difficult here in load(),
			// but it allows the code to be simple enough in save().
			
			if (fileArch.isEmpty()) {
				if (archiveArch.isEmpty()) {
					// The simplest situation: just create a "key=value" pair
					section->attr[key] = value;
					
				} else {
					// When "Archives(arch)" is given, create a pair in "Archives"
					section->archives << new ConfigArchive(archiveArch, value);
				}
			
			} else {
				// When "Files(arch)" is given, make a more complex structures
				
				// Create config[section]->files[fileArch]
				if (!section->files.contains(fileArch))
					section->files << new ConfigFilesSet(fileArch);
				
				// Set the value (containing both value and symlinking flag)
				section->files[fileArch] << new ConfigFileEntry(key, value, symlinking);
			}
		}
		else if (reMultilineAttr.exactMatch(line)) {
			// This is a «MultilineField:» line following by a multiline string
			multilineKey = reMultilineAttr.cap(1);
			section->attr[multilineKey] = "";
		}
		else if (reInclude.exactMatch(line)) {
			// We need to include sections from other files.
			// Use glob(3) to understand wildcards.
			QString pattern = reInclude.cap(1);
			const char* oldDir = get_current_dir_name();
			QDir newDir = QFileInfo(_fileName).dir();
			chdir(qPrintable(newDir.path()));
			
			glob_t buf;
			if (glob(qPrintable(pattern), 0, 0, &buf) != GLOB_NOMATCH) {
				for (int i=0; i<buf.gl_pathc; i++)
				{
					// Load other config's sections into temporary config
					QString path = buf.gl_pathv[i];
					QString absPath = newDir.filePath(buf.gl_pathv[i]);
					Config tempConfig(QDir::cleanPath(absPath), true);
					
					// Copy sections to current config
					foreach (ConfigSection section, tempConfig["_"])
						if (section.name != "_")
							*superSection << &section;
					
					// Remove sections from tempConfig to avoid deletion by destructor
					ConfigSection* meta = &tempConfig["_"]["_"];
					tempConfig["_"].remove("_");
					delete meta;
					tempConfig["_"].clear();
				}
			}
			
			// Go back from file's directory
			chdir(oldDir);
		}
	}
}

void Config::save()
{
	// Remove old data
	QFile *file = new QFile(_fileName);
	file->remove();

	// Open file
	if (!file->open(QIODevice::WriteOnly))
		throw new std::logic_error("Cannot open file for writing");
	QTextStream out(file);
	
	// Iterate through supersetions (distribution names)
	foreach (const ConfigSuperSection& superSection, *this)
	{
		// Write supersection title
		if (superSection.name != "_")
			out << "[[ " << superSection.name << " ]]\n\n";
		
		// Iterate through sections (package names)
		for (int s=0; s<superSection.count(); s++) {
			const ConfigSection& section = superSection.at(s);
			
			// Don't write anything if it is empty
			if (section.attr.isEmpty() && section.files.isEmpty() && section.archives.isEmpty())
				continue;
			
			// Write section title
			out << "[" << section.name << "]\n";
			
			// Iterate through attributes
			foreach (QString key, section.attr.keys()) {
				QVariant value = section.attr[key];
				
				// Omit "Type" attribute when it is default
				if (key == "Type" && value == "CustomPackage")
					continue;
				
				// Write "key=value", "key:\n\tmultiline", "key=value1,value2,value3".
				// In any case, omit the line if the string/list is empty.
				
				if (value.type() == QVariant::StringList) {
					// It's a list of strings
					QStringList list = value.toStringList();
					if (!list.isEmpty())
						out << key << " = " << list.join(", ") << "\n";
					
				} else {
					// It's a string (single-line or multiline)
					QString string = value.toString().trimmed();
					if (!string.isEmpty()) {
						if (string.contains('\n'))
							out << key << ":\n\t" << string.replace("\n", "\n\t") << "\n";
						else
							out << key << " = " << string << "\n";
					}
				}
			}
			
			// Iterate through files in every architecture
			foreach (ConfigFilesSet filesSet, section.files) {
				foreach (ConfigFileEntry entry, filesSet) {
					// Write "target=source" or "target->source"
					out << "Files(" << filesSet.arch << ")" << entry.target
						<< (entry.symlinking ? " -> " : " = ") << entry.source << "\n";
				}
			}
			
			// Iterate through archives
			foreach (ConfigArchive archive, section.archives) {
				out << "Archives(" << archive.arch << ") = " << archive.source << "\n";
			}
			
			// End with an empty line after the setion
			out << "\n";
		}
	}
	
	// Flush and close file
	out.flush();
	delete file;
}

Config::Type Config::type()
{
	if (contains("_", "repository"))
		return Config::Repository;
	else
		return Config::Package;
}

bool Config::contains(const QString& superSectionName, const QString& sectionName)
{
	if (PseudoMap<QString,ConfigSuperSection>::contains(superSectionName))
		if ((*this)[superSectionName].contains(sectionName))
			return true;
		return false;
	return false;
}

void Config::printStructure()
{
	foreach (const ConfigSuperSection& superSection, *this) {
		OUT(" * " + superSection.name);
		foreach (const ConfigSection& section, superSection) {
			OUT(" |-- " + section.name);
//			foreach (QString key, section.attr.keys())
//				OUT("    | "+key+": "+section.attr[key].toString());
		}
	}
}

bool Config::isModified()
{
	Config other(_fileName);
	return (*this == other);
}
