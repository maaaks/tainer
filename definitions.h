// Application metainfo
#define TAINER_APP_VERSION  "1.0"

// Magicwords for config syntax
#define MAGICWORD_EVERY     "every"
#define MAGICWORD_CURRENT   "current"
